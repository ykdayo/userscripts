// ==UserScript==
// @name Volume control for VocaDB Soundcloud embeds
// @namespace vocadb-soundcloud-volume-control
// @description Force volume control on Soundcloud embeds
// @include https://vocadb.net/*
// @exclude https://vocadb.net/
// @grant none
// @require http://w.soundcloud.com/player/api.js
// ==/UserScript==

// Inspired by https://gist.github.com/propagated/78aaedfbc0c23add7691bb975b51a3ff

let volume = 25;

// Saves volume preferences between sessions
if(!localStorage.getItem('vocadb-scvc')) {
  localStorage.setItem('vocadb-scvc', volume)
} else {
  volume = localStorage.getItem('vocadb-scvc');
};

// Fortunately they use the same container in every page
const container = document.querySelector(".container-fluid > .row-fluid > .well");

// Init new players with default volume
const newEmbed = new MutationObserver(()=>{
  const scPlayers = container.querySelectorAll("iframe[src*='soundcloud.com']:not(.scvc)");
  for (const player of scPlayers) {
    player.classList.add("scvc");
    SC.Widget(player).bind(SC.Widget.Events.READY, ()=>{
      SC.Widget(player).setVolume(volume)
    })}});
newEmbed.observe(container, { childList: true, subtree: true });

// Update volume value for all soundcloud players in the page
function volumeSlider(input) {
  if (volume !== input.target.value) {
    volume = input.target.value;
    const scPlayers = container.querySelectorAll("iframe.scvc");
    for (const player of scPlayers) {
      SC.Widget(player).setVolume(volume);
    }}};

// Add on-hover volume bar in the bottom right corner of the page
const volumeControl = document.createElement("div");
volumeControl.classList.add("scvc-container");
volumeControl.innerHTML = `<input id="scvc-slider" type="range" min="0" max="100" step="1">`;
document.body.appendChild(volumeControl);
vcInput = document.getElementById("scvc-slider");
vcInput.value = volume;
vcInput.addEventListener("input", volumeSlider);
vcInput.addEventListener("change", volumeSlider);

// Styling
const css = document.createElement("style");
document.head.appendChild(css);
css.innerHTML = `
.scvc-container {
  position: fixed;
  padding: 10px;
  padding-left: calc(10px + 40px);
  bottom: 0;
  right: -226px;
  z-index: 1030;
  background-color: rgba(25,25,25,.7);
  -webkit-transition: right 0.2s 0s ease-in;
  -moz-transition: right 0.2s 0s ease-in;
  -o-transition: right 0.2s 0s ease-in;
  transition: right 0.2s 0s ease-in;
}
.scvc-container::before {
  content: '';
  display: block;
  width: 40px;
  height: 40px;
  background: chocolate;
  position: absolute;
  top: 0;
  left: 0;
}
.scvc-container:hover {
  right: 0!important;
}
`;
