// ==UserScript==
// @name MAL Timezone Converter
// @description Converts JST broadcast time into your timezone
// @include https://myanimelist.net/anime/*/*
// @include https://myanimelist.net/anime/season*
// @require https://momentjs.com/downloads/moment.min.js
// @require https://momentjs.com/downloads/moment-timezone-with-data.min.js
// @grant none
// ==/UserScript==c

// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
// If timezone isn't guessed correctly you can manually set it
// Example: var myZone = "America/Toronto"
let myZone = "";

function isSeason() {
    if (document.body.classList.contains("season")) {
        return true
    }
}

function localTZ(s) {
    let format;
    if (isSeason()) { //  Oct 1, 2020, 23:30 (JST)
        format = "MMM D, YYYY, HH:mm (z)"
    } else { // Thursdays at 23:30 (JST)
        format = "dddd[s at] HH:mm (z)"
    }
    if (myZone === "") { myZone = moment.tz.guess() };
    const converted = moment.tz(s, format, "Asia/Tokyo");
    const formatted = converted.tz(myZone).format(format);

    return formatted.toString()
}

if (isSeason()) {
    // current season list
    const list = document.querySelectorAll(".seasonal-anime-list > .seasonal-anime");
    list.forEach(item => {
        const broadcast = item.querySelector(".remain-time");
        if (/\(JST\)/.test(broadcast.innerText)) {
            // if Japanese anime/airing time
            broadcast.setAttribute("title", localTZ(broadcast.innerText.trim()))
        }
    })
} else {
    // show specific page
    const infoSidebar = document.querySelectorAll("#content .spaceit_pad");
    infoSidebar.forEach(item => {
        if (/Broadcast:/.test(item.innerText)) {
            item.setAttribute("title", localTZ(item.innerText.replace("Broadcast:", "")))
        }
    })
}