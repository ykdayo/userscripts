// ==UserScript==
// @name        4chan X - QR Filename Randomizer
// @version     1.0
// @namespace   4chan-X-filename-randomizer
// @description Add manual filename randomizer in the QR filename field
// @include     https://boards.4chan.org/*/thread/*
// @include     https://boards.4channel.org/*/thread/*
// @grant       none
// @run-at      document-start
// @supportURL  https://gitlab.com/ykk0t/userscripts
// ==/UserScript==

(function() {
    document.addEventListener("4chanXInitFinished", initTweaks);
    document.addEventListener("QRDialogCreation", initTweaks);

    function initTweaks() {
        if (!(document.getElementById("qr"))) return;
        if (document.getElementById("qr-randomize-label")) return;

        let oldFilename;
        const container = document.getElementById("qr-filename-container"),
            fileName = document.getElementById("qr-filename"),
            fileRm = document.getElementById("qr-filerm");

        const randomizer = document.createElement("label");
        randomizer.id = "qr-randomize-label";
        randomizer.style.marginRight = "3px";
        randomizer.innerHTML = `
        <input type="checkbox" id="qr-file-random" title="Randomize filename">
        <a class="checkbox-letter">R</a>
        `;
        container.insertBefore(randomizer, fileName.nextElementSibling);

        document.getElementById("qr-file-random").onchange = (e) => {
            if (e.target.checked) {
                oldFilename = fileName.value;
                $.DAY = 24 * ($.HOUR = 60 * ($.MINUTE = 60 * ($.SECOND = 1000)));
                fileName.value = (Date.now() - Math.floor(Math.random() * 365 * $.DAY))
            } else {
                fileName.value = (oldFilename) ? oldFilename : null
            }
        }

        // Reset name field and randomizer checkbox on success or removal of file
        document.addEventListener("QRPostSuccessful", resetRandomizer);
        fileRm.addEventListener("click", resetRandomizer);
        function resetRandomizer() {
            oldFilename = null;
            document.getElementById("qr-file-random").checked = false;
        }

        const head = document.head || document.getElementsByTagName("head")[0];
        const css = document.createElement("style");
        css.innerHTML = `
            #file-n-submit:not(.has-file) #qr-randomize-label { display: none; }
        `;
        head.appendChild(css)
    }
})()


