// ==UserScript==
// @name        Warosu - YouTube Playlist
// @version     1.0
// @namespace   warosu-yt-playlist
// @description Wraps all YouTube links within an archived thread into an embedded playlist
// @include     https://warosu.org/*/thread/*
// @grant       none
// @run-at      document-end
// @supportURL  https://gitlab.com/ykk0t/userscripts
// ==/UserScript==

// Outdated

// Updated version: https://github.com/Hash6232/4chan-x-yt-playlist

(() => {
    const container = document.getElementById("postform");
    const op = container.querySelector(":scope > .content > div:first-child");
    const original = [...op.querySelectorAll("a")].find((a) => a.textContent === "Original");
    
    // Append YouTube playlist toggle
    original.nextElementSibling.insertAdjacentHTML("beforebegin", 
        "[<a id=\"playlist-toggle\" href=\"javascript:;\">Playlist</a>]");
    const toggle = original.nextElementSibling;
    toggle.addEventListener("click", togglePlaylist);

    // Add styling to <head>
    document.head.insertAdjacentHTML("beforeend", '<style>#yt-playlist {position: fixed; bottom: 0; right: 0;}.hide {display: none;}</style>');

    // Find YouTube links in the thread
    const links = [...op.parentNode.querySelectorAll("a[rel]")]
        .map((l) => getYouTubeId(l.href)).filter((l) => l !== null);

    window.onYouTubeIframeAPIReady = function() {
        let freezeCheck = false;
        const player = new YT.Player("yt-playlist", {
            width: "512",
            height: "288",
            fs: 0,
            modestbranding: 1,
            playlist: links,
            events: {
                "onReady": () => {
                    freezeCheck = true;
                    const refresh = window.setInterval(() => {
                        if (freezeCheck === false) return clearInterval(refresh);
                        player.cuePlaylist(links)
                    }, 250)
                },
                "onStateChange": (e) => {
                    e.data > -1 || (freezeCheck = false)
                }
            }
        })
    }

    // Functions
    function togglePlaylist() {
        if (links.length < 1) return;

        if (document.getElementById("yt-playlist")) {
            // toggle view
            document.getElementById("yt-playlist").classList.toggle("hide");
        } else {
            initAPI();
        }
    }

    function getYouTubeId(s) {
        if (typeof s !== "string") return null;

        // https://stackoverflow.com/a/37704433
        const regex = /^(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube(?:-nocookie)?\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)((?!playlist|shorts)[\w\-]+)/;
        return regex.test(s) ? s.match(regex)[1] : null
    }

    function initAPI() {
        const script = document.createElement("script");
        script.src = "https://www.youtube.com/iframe_api";

        document.body.insertAdjacentHTML("beforeend", '<div id="yt-playlist"></div>');

        console.debug(links);

        return document.head.appendChild(script);
    }
})()
