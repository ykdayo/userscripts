// ==UserScript==
// @name        Yotsuba Stock Exchange Tweaks
// @version     1.0
// @namespace   yotsuba-stock-exchange-tweaks
// @match       https://boards.4chan.org/yse.html
// @grant       none
// @run-at      document-end
// ==/UserScript==

const amount = document.getElementById("js-amount");

const css =  document.createElement("style");
document.head.appendChild(css);
css.textContent = "#js-summary ul li {cursor:pointer;}#js-summary ul::after {content: 'Alt+click on a label to use quick actions: \\A Clicking on your balance will quick-pump current stock tab \\A Clicking on a stock label will quick-dump everything from it';display: block;font-weight: bold;white-space:pre;}.pump,.dump{color:white;border-radius: 4px;}.pump{background-color:#c41e3a;}.dump{background-color:#00a550;}";

// Update trends on page refresh/manual update
const ob = new MutationObserver(() => {
    updateTrends();
}); ob.observe(document.getElementById("stock-list"), {childList: true});

// Auto update stock values every 60 seconds
window.setInterval(() => {
    if ((Date.now() / 1000) - YSE.lastUpdated < 60) return;
    try {
        YSE.loadPrices().then(() => updateTrends());
    } catch (err) {
        console.info(err)
    }
}, 1000 * 5);

// Add quick shortcuts on summary labels
document.getElementById("js-summary").onclick = quickAction;

function quickAction(e) {
    if (! e.altKey) return;
    if (e.target.tagName !== "LI") return;

    const label = e.target.textContent.split("×")[0];

    if (label == "$") {
        document.getElementById("js-buy-cb").checked = true;
        amount.value = Math.floor(YSE.currentBalance.balance / YSE.getLastPrice(YSE.activeStock));
        YSE.udpateOrderTotal();
    } else {
        document.getElementById("js-sell-cb").checked = true;
        YSE.setActiveStock(label);
        amount.value = YSE.currentBalance[label];
        YSE.udpateOrderTotal();
    }
}

function updateTrends() {
    const buttons = document.getElementById("stock-list");
    for (const [stock, history] of Object.entries(YSE.currentPrices)) {
        switch (stockHealth(history.slice(-3))) {
            case 1:
                buttons.querySelector("[data-s=" + stock + "]").classList.add("dump");
                buttons.querySelector("[data-s=" + stock + "]").classList.remove("pump");
                break;

            case 0:
                buttons.querySelector("[data-s=" + stock + "]").classList.remove("dump");
                buttons.querySelector("[data-s=" + stock + "]").classList.remove("pump");
                break;

            case -1:
                buttons.querySelector("[data-s=" + stock + "]").classList.add("pump");
                buttons.querySelector("[data-s=" + stock + "]").classList.remove("dump");
                break;
        }
    }

    function stockHealth(h) {
        return (h[0] < h[1] && h[1] < h[2]) ? 1 : (h[0] > h[1] && h[1] > h[2]) ? -1 : 0
    }
}