// ==UserScript==
// @name        4chan X - Page 10 Bump Helper
// @version     0.1
// @namespace   4chan-X-page-10-bump-helper
// @description This script alerts you when a thread in your 4chan X Thread Watcher is about to die
// @include     https://boards.4chan.org/*/thread/*
// @include     https://boards.4channel.org/*/thread/*
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_deleteValue
// @grant       GM_addValueChangeListener
// @grant       GM_notification
// @grant       GM_openInTab
// @run-at      document-start
// @supportURL  https://gitlab.com/ykk0t/userscripts
// ==/UserScript==

(() => {

    "use strict";

    const OP = {}, timer = [];

    // Cache watched threads list
    const watchedThreads = GM_getValue("4chan X.Watched Threads", {});
    GM_addValueChangeListener("4chan X.Watched Threads", (_k,_o, newValue, remote) => {
        if ( remote === false ) return;

        Object.assign(watchedThreads, (newValue === undefined ? {} : newValue));
    });

    document.addEventListener("4chanXInitFinished", () => {
        // Init current thread info
        [document.querySelector(".opContainer").getAttribute("data-full-i-d")]
        .map(uID => Object.assign(OP, { id: +uID.split(".")[1], board: uID.split(".")[0] }));

        // Wait for new notifications requests
        GM_addValueChangeListener("4chan X.Notification Payload", sendNotification);

        checkWatchedThreads();

        document.addEventListener("ThreadWatched", (e) => {
            [e.detail.addedThreads, e.detail.removedThreads].forEach((array, index) => {
                array.forEach(uID => {
                    const thread = { id: +uID.split(".")[1], board: uID.split(".")[0] };
                    if ( thread.board in watchedThreads === false ) watchedThreads[thread.board] = [];
                    if ( index === 0 ) {
                        // Add
                        if ( watchedThreads[thread.board].some(t => t.id === thread.id) ) return;
                        watchedThreads[thread.board].push({ id: thread.id, cooldown: false });
                    } else {
                        // Remove
                        if ( watchedThreads[thread.board].some(t => t.id === thread.id) === false ) return;
                        const index = watchedThreads[thread.board].find(t => t.id === thread.id);
                        watchedThreads[thread.board].splice(watchedThreads[thread.board].indexOf(index), 1);
                    }
                });
                GM_setValue("4chan X.Watched Threads", watchedThreads);
            })
        })
    });

    // Keep focus state updated
    document.addEventListener("visibilitychange", () => {
        const currentValue = GM_getValue("4chan X.Is Focused", 0);
        GM_setValue("4chan X.Is Focused", 
            document.visibilityState === "hidden" 
            ? ( currentValue < 1 ? 0 : currentValue - 1 ) 
            : currentValue + 1
        )
    });

    // Functions

    function checkWatchedThreads() {

        // Do NOT run if less than 60 seconds elapsed since last Fetch
        const lastFetch = GM_getValue("4chan X.Since Fetch", 0),
            currentFetch = Math.floor(new Date().getTime());
        if ( (currentFetch - lastFetch) / 1000 < 60 ) {
            // Start new loop
            while ( timer.length > 0 ) clearInterval( timer.pop() );
            return timer.push(setTimeout(checkWatchedThreads, 60 * 1000));
        };

        // Set other instances on cooldown while using Fetch
        GM_setValue("4chan X.Since Fetch", currentFetch);

        // Send one Fetch request for each board in Watched Threads
        Promise.allSettled([...Object.keys(watchedThreads)
            .map((key) => fetchCatalog(key, watchedThreads[key]))
        ])
        .then((stack) => {

            // Parse Fetch results
            stack.forEach(chunk => chunk.value.forEach(response => {
                const res = findThread(response);

                if ( res.status === "fulfilled" ) {
                    const thread = res.value.data;
                    if ( thread.cooldown === true ) return;
                    GM_setValue("4chan X.Notification Payload", res.value);
                    thread.cooldown = true;
                } else {
                    const thread = res.reason.data;
                    if ( res.reason[404] === true ) {
                        // Delete thread from storage
                        const board = watchedThreads[res.reason.board];
                        board.splice(board.indexOf(thread), 1);
                    } else {
                        thread.cooldown = false;
                    }
                }

                GM_setValue("4chan X.Watched Threads", watchedThreads);
            }));

            // Start new loop
            while ( timer.length > 0 ) clearInterval( timer.pop() );
            timer.push(setTimeout(checkWatchedThreads, 60 * 1000));
        })
    }

    function findThread(res) {
        return Array.isArray(res.value) ? findThread(res.value) : res
    }

    async function fetchCatalog(boardName, boardThreads) {
        // https://github.com/4chan/4chan-API/blob/master/pages/Threadlist.md
        return fetch(`https://a.4cdn.org/${boardName}/threads.json`)
        .then(res => res.json())
        .then(catalog => Promise.allSettled([...boardThreads
            .map(thread => parseThread(boardName, catalog, thread))
        ]))
    }

    function parseThread(board, pages, thread) {
        return new Promise((needsBump, reject) => {
            if ( pages.some(p => p.threads.some(t => t.no == thread.id)) === false ) {
                reject({ 404: true, board: board, data: thread })
            } else {
                const matchingPage = pages.find(p => p.threads.some(t => t.no == thread.id));
                const payload = { page: matchingPage.page, board: board, data: thread };
                +matchingPage.page >= 7 ? needsBump( payload ) : reject( payload ) ;
            }
        })
    }

    function sendNotification(_k,_o, p, remote) {
        if ( p === undefined ) return;
        GM_deleteValue("4chan X.Notification Payload");

        const message = document.createElement("div"),
            link = new URL(`${p.board}/thread/${p.data.id}`, "https://boards.4chan.org");
        message.innerHTML = `Thread <a href="${link}" target="_blank">/${p.board}/${p.data.id}</a> is on page ${p.page}.`;

        if ( GM_getValue("4chan X.Is Focused", 0) > 0 ) {
            if ( document.hidden === true ) return;

            document.dispatchEvent(new CustomEvent('CreateNotification', {
                bubbles: true, 
                detail: {
                    type: "warning",
                    content: message
                }
            }))
        } else {
            if ( remote === true ) return;

            const HTML5 = GM_notification({
                text: message.innerText,
                title: "4chan X",
                image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAACVBMVEUAAGcAAABmzDNZt9VtAAAAAXRSTlMAQObYZgAAAF5JREFUeNrtkTESABAQxPD/R6tsE2dUGYUtFJvLDKf93KevHJAjpBorAQWSBIKqFASC4G0pCAkm4GfaEvgYXl0T6HBaE97f0vmnfYHbZOMLZCx9ISdKWwjOWZSC8GYm4SUGwfYgqI4AAAAASUVORK5CYII=",
                onclick: () => {
                    GM_openInTab(link.toString(), { active: true })
                }
            });
            setTimeout(() => { HTML5.remove() }, 10 * 1000)
        }
    }
})()
