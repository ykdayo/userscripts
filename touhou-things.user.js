// ==UserScript==
// @name Touhou things - Light encode links for sharing
// @namespace touhou-things-uri-light-encoder
// @description Generates a partially encoded url to share that is not as lengthy as a fully URI encoded one
// @include http://151.80.40.155/*
// @grant none
// ==/UserScript==

let decodedHash, encodedHash;
const browseBar = document.getElementById("breadcrumb"),
    cShortlink = document.createElement("li"),
    exportShortlink = document.createElement("a");

cShortlink.id = "shortlink";
cShortlink.appendChild(exportShortlink);
exportShortlink.innerText = "Share shorter link";
exportShortlink.style.cursor = "pointer";
exportShortlink.addEventListener("click", (e)=>{
    e.preventDefault();
    alert("http://" + location.host + "/" + e.target.getAttribute("data-hash"))
});

const breadObserver = new MutationObserver( () => {
    console.debug("Check for loops");
    if (browseBar.querySelector(".active")) {
        updateHash();
        if (!document.getElementById("shortlink")) {
            browseBar.appendChild(cShortlink);
        }
        breadObserver.disconnect();
    }
});

// Ugly init for when the address already contains an hash
breadObserver.observe(browseBar, { childList: true });

window.onhashchange = function() {
    breadObserver.observe(browseBar, { childList: true });
};

function updateHash() {
    if (location.hash !== "<empty string>") {
        decodedHash = decodeURIComponent(location.hash);
        encodedHash = decodedHash.replace(/ /g, "%20");
        encodedHash = encodedHash.replace(/　/g, "%E3%80%80");
        exportShortlink.setAttribute("data-hash", encodedHash);
    }
}
