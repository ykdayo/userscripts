// ==UserScript==
// @name        4chan X - Filters for generals
// @version     1.0
// @namespace   4chan-X-generals-filters
// @description Hide posts if their highlight-class matches the one in the OP
// @include     https://boards.4chan.org/*/thread/*
// @include     https://boards.4channel.org/*/thread/*
// @grant       none
// @run-at      document-start
// @supportURL  https://gitlab.com/ykk0t/userscripts
// ==/UserScript==

// How to setup? Examples below:
// Add this in Subject -> boards:4:vg;top:no;highlight:my-general;/Content of the Subject/i
// Add this in Comment -> boards:4:vg;top:no;highlight:my-general-filter;op:no;/Your filter/i

(function() {
  document.addEventListener("4chanXInitFinished", function (e) {
    const thread = document.querySelector(".thread"),
          op = thread.querySelector(".opContainer");

    hidePosts(thread.querySelectorAll(".postContainer[class*=-filter]"));
    document.addEventListener("ThreadUpdate", function (e) {
      if (e.detail[404] === false) {
        if (e.detail.newPosts.length > 0) {
          hidePosts(e.detail.newPosts);
        }
      }
    })

    function hidePosts(list) {
      list.forEach((item) => {
        let post, id;
        if (Array.isArray(list)) { // new posts
          id = item;
          post = thread.querySelector(`.postContainer[id$="${id.split(".").pop()}"]`);
        } else { // thread init
          post = item;
          id = post.getAttribute("data-full-i-d")
        }

        // Don't filter posts you make
        if (post.classList.contains("yourPost")) return;

        op.classList.forEach(c => {
          if (!post.classList.contains(`${c}-filter`)) return;
          post.querySelector(".replacedSideArrows > a").click();
        })
      })
    }
  })
})();