// ==UserScript==
// @name        Pixiv Image Proxy
// @namespace   pixiv-image-proxy
// @match       https://i.pximg.net/img-original/img/*/*/*/*/*/*/*_p*.*
// @grant       none
// @author      -
// @description Redirect pixiv image link to pixiv.cat proxy
// @run-at      document-start
// ==/UserScript==

(function() {
  const pic = window.location.href.match(/(\d+)_p\d+(\.\w{3,4})$/);
  window.location.href = "https://pixiv.cat/" + pic[1] + pic[2];
})()