// ==UserScript==
// @name        Mirapri's EN Filters
// @namespace   mirapri-english-filters
// @match       https://mirapri.com/*
// @grant       none
// @author      steponmepls
// @description Add non-intrusive english translations to filter fields
// @run-at      document-end
// ==/UserScript==

(function() {
  const filterMenu = document.getElementById("searchMenu"),
    filterList = filterMenu.querySelectorAll("form.filter-form > nav");

  filterList.forEach(filter => {
    let translations;
    const buttons = filter.querySelectorAll("a.menu-button");
    switch(filter.children[0].textContent) {
      case "KEYWORD":
        document.getElementById("equip-name-field").setAttribute("placeholder", "Input item name");
        break;
      case "SORT":
        let periods = [ "Any", "Today", "This week", "This month", "This year" ],
          criteria = [ "Any", "By likes", "By views" ];
        document.getElementById("period").querySelectorAll("option").forEach((item, i) => {
          item.innerText = periods[i];
        });
        document.getElementById("sort").querySelectorAll("option").forEach((item, i) => {
          item.innerText = criteria[i];
        });
        break;
      case "GENDER":
        translations = [ "Male", "Female", "Unisex" ];
        break;
      case "CLASS":
        translations = [ "Physical", "Caster", "Crafter", "Gatherer" ];
        break;
      case "JOB":
        translations = [ "PLD", "WAR", "DRK", "DRG", "MNK", "NIN", "SAM", "BRD", "MCH", "BLM", "RDM", "SMN", "SCH", "WHM", "AST", "BLU", "DNC", "GNB", "SGE", "RPR"];
        break;
      case "RACE":
        translations = [ "Hyur", "Elezen", "Miqote", "Lalafell", "Roegadyn", "Au Ra", "Viera", "Hrothgar", "Chocobo" ];
        break;
      case "COLOR":
        translations = [ "White/Black", "Red", "Brown", "Yellow", "Green", "Blue", "Purple", "Undyed" ];
        break;
      case "FAV":
        translations = [ "Bookmarked only" ];
        break;
    };
    buttons.forEach((button, i) => {
      button.setAttribute("title", translations[i])
    })
  });

  const dbLinks = document.querySelectorAll("a.eorzeadb_link");
  if (dbLinks.length < 1) return;

  dbLinks.forEach(link => {
    link.href = link.href.replace("jp.finalfantasyxiv", "na.finalfantasyxiv")
  })
})()
