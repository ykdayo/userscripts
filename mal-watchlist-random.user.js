// ==UserScript==
// @name        MAL - Random anime from watchlist
// @namespace   mal-watchlist-random
// @match       https://myanimelist.net/animelist/*
// @grant       none
// @description Picks a "random" anime from your personal MAL watchlist
// @run-at      document-end
// ==/UserScript==

(function() {
    "use strict";

    if (document.body.getAttribute("data-owner") === "" || document.body.getAttribute("data-query") !== `{"status":6}`) return;

    const rContainer = document.createElement("span"),
        rButton = document.createElement("a"),
        header = document.querySelector(".list-status-title");
    
    rContainer.appendChild(rButton);
    header.insertBefore(rContainer, header.querySelector("span:first-of-type"));
    rButton.innerHTML = `<i class="fa-solid fa-shuffle"></i> Random anime`;
    
    rContainer.style = "position: absolute; height: 38px; line-height: 38px; left: 4px";
    // Match link color of other links in .stats node
    rButton.style = `margin-left: 8px; color: ${window.getComputedStyle(header.querySelector(".stats > a")).getPropertyValue("color")}`;
    rButton.href = "javascript: void(0)";
    
    rButton.onclick = (e) => {
        e.preventDefault();
    
        const list = Array.from(document.querySelectorAll(".list-table > tbody[class]"))
                    .filter(entry => {if (entry.querySelector("td.data.title span.content-status").innerText !== "Not Yet Aired") return entry}),
              randomNode = list[Math.floor(Math.random() * list.length) + 1], // Random number between 1 and list.length
              link = randomNode.querySelector("td.title > a:first-of-type").href;
    
        window.location = link;
    }
})()
