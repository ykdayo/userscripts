// ==UserScript==
// @name            Download all as zip for Imgur
// @namespace       imgur-zip-it
// @description     Batch downloads all the pictures in an album as a zip archive
// @match           https://imgur.io/a/*
// @require         https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js
// @grant           none
// @run-at          document-end
// ==/UserScript==

// It's useless. You should move from imgur.io to imgur.com and just download the gallery as zip like that
// Also kinda broken because it sometimes skips an image during the blob generation. Dunno why

(async function() {
  let customId = ""; // Enter your custom imgur app ID here if you have one
  const id = customId !== "" ? customId : "9dcbfd17b9635e5"; // Fallback

  // Fetch album content
  const hash = window.location.href.split("/a/").pop(),
    list = await fetch(`https://api.imgur.com/3/album/${hash}/images`, {
      headers: { Authorization: `Client-ID ${id}` }
    }).then(res => res.json());

  // Add download link to page
  const dl = document.createElement("a");
  dl.id = "imgur-zip-it";
  dl.textContent = "Download";
  dl.href = "javascript:();";
  dl.style = "position: absolute; top: 50px; left: 4px; z-index: 9001";
  document.body.appendChild(dl);
  dl.onclick = (e) => {
    e.preventDefault();
    downloadAlbum()
  };

  async function downloadAlbum() {
    const zip = new JSZip(),
      album = zip.folder(hash);

    Array.from(list.data).forEach(async function(image, i, arr) {
      const name = `${i + 1} - ${image.id}.${image.link.split(".").pop()}`,
        blob = await fetch(image.link).then(response => response.blob()),
        imgData = new File([blob], name);
      album.file(name, imgData, { base64: true });

      if (i < arr.length - 1) return;
      zip.generateAsync({ type: 'blob' }).then(function(content) {
        saveAs(content, `Imgur Album.zip`);
      });
    })
  }
})()