// ==UserScript==
// @name        4chan X - YouTube Playlist
// @version     1.0
// @namespace   4chan-X-yt-playlist
// @description Wraps all YouTube links within a thread into an embedded playlist
// @include     https://boards.4chan.org/*/thread/*
// @include     https://boards.4channel.org/*/thread/*
// @grant       none
// @run-at      document-start
// @supportURL  https://gitlab.com/ykk0t/userscripts
// ==/UserScript==

// Outdated

// Updated version: https://github.com/Hash6232/4chan-x-yt-playlist

(function() {

  "use strict";

  const thread = {}, updateIntervals = [];
  let pagedIds, YTplaylist, cTrack, cLength, cIndex = 0;
  let needsUpdate = false, isUpdating = false,
      freezeCheck = false, isPlaying = false;
  
  document.addEventListener("4chanXInitFinished", (e) => {
    Promise.allSettled([...Array.from(document.querySelectorAll(".thread .postContainer"))
      .filter(post => post.querySelector("a.linkify.youtube"))
      .map(post => getLinks(post.getAttribute("data-full-i-d")))
    ]).then((res) => {
      // Prevent double update
      needsUpdate = false;
  
      // Generate playlist pages
      pagedIds = splitPlaylist();
      document.dispatchEvent(new CustomEvent("UpdatePagination"));
  
      // Only init API if non empty response from Promise
      if (res.length < 1) return;
  
      initAPI()
    });
  
    // Topbar toggle
    const dToggle = document.createElement("span"),
          qrToggle = document.getElementById("shortcut-qr");
    dToggle.id = "shortcut-playlist";
    dToggle.classList.add("shortcut");
    qrToggle.parentNode.insertBefore(dToggle, qrToggle);
    dToggle.innerHTML = `
      <a class="fa fa-window-maximize disabled" 
      title="Toggle embed dialog" 
      href="javascript:;">Embed</a>`;
    dToggle.querySelector("a").onclick = (e) => {
      e.preventDefault();
      const dialog = document.getElementById("embedding");
      if (dialog.classList.contains("has-embed") || 
          dialog.classList.contains("has-playlist")) {
        dialog.classList.toggle("empty");
        e.target.classList.toggle("disabled")
      } else {
        if (isUpdating) {
          document.dispatchEvent(new CustomEvent("CreateNotification", {
            detail: {
              type: "warning", 
              content: "Playlist is still being processed!", 
              lifetime: 3 
            }
          }))
        } else if (pagedIds && pagedIds.length < 1) {
          document.dispatchEvent(new CustomEvent("CreateNotification", {
            detail: {
              type: "warning", 
              content: "No valid YouTube links in this thread.", 
              lifetime: 3 
            }
          }))
        } else {
          failedToload()
        }
      }
    };

    // Embed dialog tabs init
    const topBar = document.getElementById("embedding").querySelector("div:first-child");
    const tabList = document.createElement("ul");
    tabList.innerHTML = `
      <li class="tab main">Main</li><li class="tab playlist active">Playlist</li>
    `;
    topBar.insertBefore(tabList, topBar.querySelector(".move"));
    tabList.id = "embed-tabs";
    tabList.querySelectorAll("li").forEach(i => {
      i.onclick = (e) => {
        if (e.target.classList.contains("active")) return;
  
        const sibling = e.target.previousElementSibling || e.target.nextElementSibling;
        sibling.classList.remove("active");
        e.target.classList.add("active");
  
        document.getElementById("embedding").classList.toggle("show-playlist");
      }
    });
  
    // Custom jumpTo init
    const lJumpTo = document.querySelector("#embedding a.jump"),
          pJumpTo = lJumpTo.cloneNode(true);
    pJumpTo.classList.add("playlist");
    lJumpTo.parentNode.insertBefore(pJumpTo, lJumpTo);
    pJumpTo.onclick = (e) => {
      e.preventDefault();
      const id = Object.entries(thread).find(post => post[1].includes(cTrack))[0];
      document.getElementById(`pc${id.split(".").pop()}`).scrollIntoView();
    };
  
    // Pagination init
    const ePager = document.createElement("ul");
    ePager.id = "playlist-pages";
    pJumpTo.parentNode.insertBefore(ePager, pJumpTo);

    // Class toggle handler AKA click on (embed) links
    const embedEvent = new MutationObserver((e) => {
      const mTab = tabList.querySelector(".tab.main"),
            pTab = tabList.querySelector(".tab.playlist");
      if (e[0].addedNodes[0].classList.contains("media-embed")) {
        e[0].target.parentNode.classList.add("has-embed");
        e[0].target.parentNode.classList.remove("show-playlist");
        mTab.classList.add("active");
        pTab.classList.remove("active");
        dToggle.querySelector("a").classList.remove("disabled")
      } else {
        e[0].target.parentNode.classList.remove("has-embed");
        e[0].target.parentNode.classList.add("show-playlist");
        mTab.classList.remove("active");
        pTab.classList.add("active");
        dToggle.querySelector("a").classList.add("disabled");
      }
    });
    embedEvent.observe(document.getElementById("media-embed"), {
      childList: true
    });
  
    // Styling
    const css = document.createElement("style");
    document.head.appendChild(css);
    css.innerHTML = `
      #embed-tabs,
      #playlist-pages {
        font-family: monospace;
        text-transform: uppercase;
        list-style: none;
        margin: 0;
        padding: 0;
      }
      #embed-tabs li,
      #playlist-pages li {
        display: inline-block;
        cursor: pointer;
      }
      #embed-tabs li {
        margin-right: 8px;
      }
      #playlist-pages li {
        margin-right: 4px;
      }
      #embed-tabs li:not(.active) {
        opacity: .7;
      }
      #embed-tabs li.active::before {
        content: '>';
      }
      #embed-tabs li.active::after {
        content: '<';
      }
      #embedding:not(.has-embed) #embed-tabs,
      #embedding:not(.has-playlist) #embed-tabs,
      #embedding:not(.show-playlist) #playlist-pages,
      #embedding.show-playlist a.jump:not(.playlist),
      #embedding:not(.show-playlist) a.jump.playlist,
      #embedding.show-playlist #media-embed,
      #embedding:not(.show-playlist) #playlist-embed {
        display: none;
      }
    `;
  
    // Events
    document.addEventListener("ThreadUpdate", (e) => {
      if (e.detail[404] === true) return;
  
      const promises = [];
  
      if (e.detail.newPosts.length > 0) {
        e.detail.newPosts.forEach(uID => {
          const post = document.getElementById(`m${uID.split(".").pop()}`);
          if (!post.querySelector("a.linkify.youtube")) return;
          promises.push(getLinks(uID));
        })
      };
  
      if (e.detail.deletedPosts.length > 0) {
        e.detail.deletedPosts.forEach(uID => {
          if (Object.keys(thread).includes(uID) === false) return;
          delete thread[uID];
          needsUpdate = true;
        })
      };
  
      Promise.allSettled(promises).then(() => {
        isUpdating = false;
        if (isPlaying || !needsUpdate) return;
        document.dispatchEvent(new CustomEvent("UpdatePlaylist"));
      })
    });
    document.addEventListener("UpdatePlaylist", (e) => {
      pagedIds = splitPlaylist();
      document.dispatchEvent(new CustomEvent("UpdatePagination"));
  
      pagedIds.forEach((page, pIndex, pages) => {
        if (page.includes(cTrack) === false) return;
  
        let callback;
        const trackLength = YTplaylist.getDuration(),
              trackTime = YTplaylist.getCurrentTime(),
              vPage = cIndex === 199 && pIndex < pages.length - 1 ? page + 1 : page, 
              vIndex = trackTime === trackLength && trackLength > 0 ? cIndex + 1 : cIndex,
              vTime = trackTime < trackLength ? trackTime : null;
  
        if (isPlaying) {
          if (!e.detail || !e.detail.ended) return;
          console.debug("loadPlaylist()");
          callback = () => YTplaylist.loadPlaylist(vPage, vIndex);
        } else {
          console.debug("cuePlaylist()");
          callback = () => YTplaylist.cuePlaylist(vPage, vIndex, vTime);
        }
  
        // Old method: https://stackoverflow.com/questions/66188481
        // Old method: https://jsfiddle.net/o0f815t3/
  
        updateIntervals.forEach((i, index, array) => {
          window.clearInterval(array.splice(array[index], 1));
        });
        freezeCheck = true;
        const interval = window.setInterval(() => {
          if (!freezeCheck) clearInterval(interval);
          callback();
        }, 500);
        updateIntervals.push(interval);
  
        needsUpdate = false
      })
    });
    document.addEventListener("UpdatePagination", (e) => {
      if (pagedIds.length < 2) return;
  
      const list = ePager.querySelectorAll("a[data-page]");
    
      // Prevent unnecessary updates
      if (list.length === pagedIds.length) return;
    
      // Reset pager content before generating new entries
      while (list.lastChild) list.removeChild(list.lastChild);
    
      pagedIds.forEach((page, index) => {
        const newPage = document.createElement("li");
        newPage.innerHTML = `
          <a href="javascript:;" data-page="${index}">${index + 1}</a>
        `;
        ePager.appendChild(newPage);
        newPage.querySelector("a").onclick = (e) => {
          e.preventDefault();
  
          // Clean up eventual timer leftovers
          updateIntervals.forEach((i, index, array) => {
            window.clearInterval(array.splice(array[index], 1));
          });
          freezeCheck = true;
          const interval = window.setInterval(() => {
            if (!freezeCheck) clearInterval(interval);
            YTplaylist.cuePlaylist(pagedIds[e.target.getAttribute("data-page")]);
          }, 500);
          updateIntervals.push(interval);
        }
      })
    });
  });
  
  // Functions
  function getLinks(uID) {
    isUpdating = true;
  
    const rawIds = Object.values(thread).flat();
    if (uID in thread === false) thread[uID] = [];
  
    const post = document.getElementById(`m${uID.split(".").pop()}`);
    return Promise.allSettled([...Array.from(post.querySelectorAll("a.linkify.youtube"))
      .map((link, i) => {
        let vID;
        if (link.pathname === "/watch") { // full link
          vID = link.search.match(/v=([A-Za-z0-9-_]+)/)[1]
        } else { // shortlink
          vID = link.pathname.match(/^\/([A-Za-z0-9-_]+)/)[1]
        };
  
        return fetch(`https://www.youtube.com/oembed?url=https://youtu.be/${vID}&format=json`, {method: "HEAD"})
          .then(async res => {
            if (res.status === 200) {
              if (!rawIds.includes(vID)) needsUpdate = true;
              thread[uID][i] = vID
            };
            return res.status
          })
      })
    ])
  }
  function splitPlaylist() {
    const rawPlaylist = [...new Set(Object.values(thread).flat())];
  
    // Split array in 200 items long pages
    const output = [];
    while (rawPlaylist.length > 0) {
      output.push(rawPlaylist.splice(0, 200))
    };
  
    if (output.length > 0) console.debug(output);
    // console.debug(output[0].toString());
    return output
  }
  function initAPI() {
    const script = document.createElement("script"),
          container = document.createElement("div");
  
    container.id = "playlist-embed"
    document.getElementById("embedding").appendChild(container);
    script.src = "https://www.youtube.com/iframe_api";
    document.head.appendChild(script);
  }
  function failedToload() {
    document.dispatchEvent(new CustomEvent("CreateNotification", {
      detail: { type: "error", content: "Unable to load YouTube Iframe API.\nPress F12 and check for errors in the console." }
    }));
    console.error("Unable to load YouTube Iframe API.\n" +
      "Remember to add the following exceptions:\n" +
      "4chanX's Settings > Advanced > Javascript Whitelist\n" +
      " https://www.youtube.com/iframe_api\n" +
      " https://www.youtube.com/s/player/\n" +
      "Filters in your AdBlock extension\n" +
      " @@||www.youtube.com/iframe_api$script,domain=4channel.org\n" +
      " @@||www.youtube.com/s/player/*$script,domain=4channel.org\n"
    )
  }
  
  // YouTube Iframe API
  window.onYouTubeIframeAPIReady = () => {
    YTplaylist = new YT.Player("playlist-embed", {
      width: '512',
      height: '288',
      playerVars: {
        'fs': 0,
        'disablekb': 1,
        'modestbranding': 1
      },
      events: {
        "onReady": function(e) {
          console.debug(thread);

          // Clean up eventual timer leftovers
          updateIntervals.forEach((i, index, array) => {
            window.clearInterval(array.splice(array[index], 1));
          });
          const interval = window.setInterval(() => {
            if (e.target.getVideoUrl() != "https://www.youtube.com/watch") clearInterval(interval);
            e.target.cuePlaylist(pagedIds[0]);
          }, 500);
          updateIntervals.push(interval);
  
          // Toggle playlist on in embed dialog
          document.getElementById("embedding").classList.add("has-playlist", "show-playlist");

          // Reset mutation check
          isUpdating = false
        },
        "onStateChange": function(e) {
          // console.debug(e.data);
          switch(+e.data) {
  
            case 0: // If playlist ended
              if (needsUpdate) {
                document.dispatchEvent(new CustomEvent("UpdatePlaylist", {
                  detail: { ended: true }
                }));
                break;
              };
  
              // Load next page if last track of the page..
              pagedIds.forEach((page, pageNum, pages) => {
                if (page.includes(cTrack) === false) return;
                // ..but NOT last page of the playlist..
                if (pageNum + 1 === pages.length) return;
  
                // Clean up eventual timer leftovers
                updateIntervals.forEach((i, index, array) => {
                  window.clearInterval(array.splice(array[index], 1));
                });
                freezeCheck = true;
                const interval = window.setInterval(() => {
                  if (!freezeCheck) clearInterval(interval);
                  e.target.loadPlaylist(pagedIds[pageNum + 1]);
                }, 500);
                updateIntervals.push(interval);
              });
  
              break;
  
            case -1: // If track ended and/or next is loaded
              if (freezeCheck) freezeCheck = false;
  
              cTrack = e.target.getVideoUrl().split("=").pop();
              cIndex = e.target.getPlaylistIndex();
              cLength = e.target.getPlaylist().length;
              // console.debug("cTrack", cTrack, "cIndex", cIndex, "cLength", cLength);
  
              // Due to a change in when state 0 is returned, update playlist
              // attempts must now also happen when the next track is loaded
              if (needsUpdate) {
                document.dispatchEvent(new CustomEvent("UpdatePlaylist", {
                  detail: { ended: true }
                }));
              };
  
              break;
          };
  
          // This has to stay at the bottom or it will mess with prev isPlaying checks
          isPlaying = (e.data == 1 || e.data == 3) ? true : false;
        },
        "onError": function(e) {
          let errLvl, errMsg;
          switch(+e.data) {
            case 5:
            case 100:
            case 101:
            case 150:
              if (cIndex < cLength) { e.target.nextVideo() };
            case 101:
            case 150:
              errLvl = "warning";
              errMsg = "The owner of the requested video does not allow it to be played in embedded players.";
              break;
            case 2:
              errLvl = "error";
              errMsg = "The request contains an invalid parameter value.";
              break;
            case 5:
              errLvl = "error";
              errMsg = "The requested content cannot be played in an HTML5 player.";
              break;
            case 100:
              errLvl = "warning";
              errMsg = "The video has been removed or marked as private.";
              break;
          };
  
          const output = "Error - Video #" + (cIndex + 1) + "\n" + errMsg;
          console.info(output);
          document.dispatchEvent(new CustomEvent("CreateNotification", {
            detail: { type: errLvl, content: output, lifetime: 10 }
          }));
        }
      }
    })
  }
})();