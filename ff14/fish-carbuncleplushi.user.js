// ==UserScript==
// @name            FFXIV Fish Tracker Autocomplete
// @namespace       ff14fish-carbuncleplushy-autocomplete
// @description     Adds autocomplete buttons to use with Import feature
// @match           https://ff14fish.carbuncleplushy.com/
// @grant           none
// ==/UserScript==

(function() {
  "use strict";

  const rawList = Object.entries(DATA.FISH), log = {}, exportedFishes = {},
    exportData = document.getElementById("import-settings-data"),
    loadData = document.getElementById("import-settings-load");

  // Organize database in patches
  for (const id of rawList) {
    // Move mini patches inside bigger ones - Ex: 5.55 > 5.5
    const patch = id[1].patch.toString().length > 3 ? id[1].patch.toFixed(1) : id[1].patch;

    if (patch in log === false) log[patch] = [];
    log[patch].push(id[0]);
  }

  // Append patch picker in Import modal
  const patchPicker = document.getElementById("filterPatch").cloneNode(true);
  patchPicker.id = "autocomplete-fishes";
  const importModal = document.getElementById("import-settings-modal");
  importModal.querySelector(".content > div").insertBefore(patchPicker,
    importModal.querySelector(".content > div > div:last-of-type")
  )

  // Init autocompletion buttons ignoring patches not yet released
  patchPicker.querySelectorAll(".button[data-filter]:not(.disabled)").forEach((button, i, list) => {
    // Make sure all buttons are toggled off on start
    button.classList.remove("active");
    button.addEventListener("click", (e) => {
      e.preventDefault();
      togglePatch(e.target, list)
    } );
  });
  exportData.value = ""; // Reset on page refresh AKA F5

  // Reset objects and classes after a successfull import
  loadData.addEventListener("click", () => {
    patchPicker.querySelectorAll(".button[data-filter]:not(.disabled)").forEach(button => {
      button.classList.remove("active");
    });
    for (let k in exportedFishes) delete exportedFishes[k];
  });

  // Styling
  const pTitle = patchPicker.querySelector("div.row:first-of-type");
  pTitle.classList.add("field");
  pTitle.style = "margin: 0";
  pTitle.innerHTML = `<label>${pTitle.textContent.replace("Filter", "Autocomplete").replace(":", "")}</label>`;
  patchPicker.querySelectorAll("div.row:not(first-child) > div").forEach(node => { node.style.width = "100%" });
  patchPicker.style.marginBottom = "1em";

  // Functions
  function togglePatch(button, siblings) {
    if (button.classList.contains("patch-set")) {
      toggleState(Array.from(siblings).filter(n => {
        if (n.getAttribute("data-filter") in log === false) return; // Skip 2.1
        if (n.classList.contains("patch-set")) return; // Remove duplicate patch ids
        if (!n.getAttribute("data-filter").startsWith(button.getAttribute("data-filter"))) return;

        return n
      }), button.classList.toggle("active"))
    } else {
      toggleState([button], button.classList.toggle("active"))
    }
    
    function toggleState(nodes, state) {
      nodes.forEach(node => {
        const filterId = node.getAttribute("data-filter");

        if (state === true) {
          node.classList.add("active");
          exportedFishes[filterId] = log[filterId];
        } else {
          node.classList.remove("active");
          delete exportedFishes[filterId];
        }
      });

      // Update Exported Data textarea with content of exportedFishes object
      if (Object.keys(exportedFishes).length > 0) {
        const flattened = `[${Object.values(exportedFishes).flat().toString()}]`;
        exportData.value = flattened;
      } else {
        exportData.value = "";
      }
    }
  }
})()